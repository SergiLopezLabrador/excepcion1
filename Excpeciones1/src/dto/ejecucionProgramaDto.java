package dto;

import exceptions.exceptionPadre;

public class ejecucionProgramaDto {

	
	//Aqu� creo el m�todo donde agrupo todos los m�todos para poder ejercer el m�todo final
	public static void ejecutarPrograma() {
		//Aqu� creo las variables
        int intentosPrograma = 1;
        int numeroUsuario;
        boolean salirBucle = false;
        int numAleatorio = numAleatorioDto.numeroRandom(1, 500);
        System.out.println(numAleatorio);
        
        //Aqu� creo un bucle tipo do/while donde no podr� salir a no ser que el booleano sea true
        do {
        	
        	numeroUsuario = exceptionPadre.tryCatch();
            
            if (OperacionesDto.mayorMenor(numeroUsuario, numAleatorio)) {
                System.out.println("Has realizado " + intentosPrograma + " intentos para poder adivinar el n�mero aleatorio");
                salirBucle = true;
            }
            
            intentosPrograma ++;
            
        }
        while (!salirBucle);
    }
}
