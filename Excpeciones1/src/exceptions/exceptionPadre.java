package exceptions;
import views.viewsPadre;

public class exceptionPadre {
	
	//Aqu� voy ha crear un try catch que repetir� la pregunta del n�mero del usuario en el caso de que no se ponga un Integer (en este caso)
	public static int tryCatch() {
		
		int numeroTryCatch = 0;
		
		try {
	        
			numeroTryCatch = viewsPadre.pregunta();

		//En el caso de que no ponga un Integer se ver� por consola este mensaje y se repetir� el try
	    }catch (NumberFormatException a) {
	        System.out.println("No has puesto un n�mero v�lido");
	    }
		
		return numeroTryCatch;
		
	}
	
}
